from django.test import TestCase
from django.contrib.auth.models import User

from . import models


class EventUserProblemTypeTestCase(TestCase):
    def setUp(self):
        self.user = User.objects.create_user('sonu', 'sonu@xyz.com', 'sn@pswrd')
        self.event = models.Event.objects.create(name='event')
        self.problem_type = models.ProblemType.objects.create(name='problem')
        self.event_user_problem_type = models.EventUserProblemType.objects.create(
            user=self.user,
            event=self.event,
            problem_type=self.problem_type
        )

    def test_event_user_problem_type(self):

        event_user_problem_type = models.EventUserProblemType.objects.get(pk=self.event_user_problem_type.pk)
        self.assertEqual(event_user_problem_type.user, self.user)
        self.assertEqual(event_user_problem_type.event, self.event)
        self.assertEqual(event_user_problem_type.problem_type, self.problem_type)
