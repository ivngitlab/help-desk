from django.conf import settings
from django.db import models


class Event(models.Model):
    name = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.name


class ProblemType(models.Model):
    name = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.name


class EventUserProblemType(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    problem_type = models.ForeignKey(ProblemType, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.user.username.capitalize()} has a problem: {self.problem_type}'
