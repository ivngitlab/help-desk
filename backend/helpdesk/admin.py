from django.contrib import admin
from django.contrib.auth.models import User

from . import models


class MembershipInline(admin.TabularInline):
    model = models.EventUserProblemType
    extra = 3


class UserAdmin(admin.ModelAdmin):
    inlines = [MembershipInline]


class EventAdmin(admin.ModelAdmin):
    inlines = [MembershipInline]


class ProblemTypeAdmin(admin.ModelAdmin):
    inlines = [MembershipInline]


class EventUserProblemTypeAdmin(admin.ModelAdmin):
    list_display = ('user', 'event', 'problem_type',)
    list_editable = ('event', 'problem_type',)

    class Meta:
        model = models.EventUserProblemType


admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(models.Event, EventAdmin)
admin.site.register(models.ProblemType, ProblemTypeAdmin)
admin.site.register(models.EventUserProblemType, EventUserProblemTypeAdmin)
