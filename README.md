# help-desk



## getting started

### environment variable setting (do not use in production)

```
export SECRET_KEY=secret 
export DJANGO_SUPERUSER_USERNAME=admin 
export DJANGO_SUPERUSER_EMAIL=admin@admin.admin 
export DJANGO_SUPERUSER_PASSWORD=admin 
```

### prepare service

```
python manage.py migrate
python manage.py createsuperuser --no-input
python manage.py loaddata fixture
```

### run test

```
python manage.py test
```

### run service

```
python manage.py runserver
```

### use of the service

`http://127.0.0.1:8000/`
